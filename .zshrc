DISABLE_AUTO_UPDATE="true"
SAVEHIST=100000
HISTSIZE=100000
HISTFILE=~/.zsh_history
source ~/.zsh/antigen.zsh

antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle zsh-users/zsh-history-substring-search

antigen apply

export DISTCC_HOSTS="localhost/5 192.168.0.65/25"
export PATH=/usr/lib/distcc/bin:~/.cargo/bin:~/.local/bin:~/git/cbc/bin:$PATH
export MAGICK_CONFIGURE_PATH=$HOME/.config/ImageMagick
export QT_QPA_PLATFORMTHEME="qt5ct"
export EDITOR="vim"
export FZF_DEFAULT_COMMAND="find ."
export ASYMPTOTE_PDFVIEWER="okular"
export ERL_AFLAGS="-kernel shell_history enabled"
export DEVKITPRO=/opt/devkitpro
export DEVKITARM=/opt/devkitpro/devkitARM
export DEVKITPPC=/opt/devkitpro/devkitPPC

alias zshconfig="vim ~/.zshrc"
alias tmux="tmux -u"
alias vimconfig="vim ~/.vimrc"
alias catkin_make="catkin_make -DPYTHON_EXECUTABLE=/usr/bin/python2 -DPYTHON_INCLUDE_DIR=/usr/include/python2.7 -DPYTHON_LIBRARY=/usr/lib/libpython2.7.so"
alias ros="source /opt/ros/lunar/setup.zsh && source ~/catkin_ws/devel/setup.zsh"
alias h="stack ghc *.hs"
alias MAKE="make"
alias reboot="sudo systemctl reboot"
alias poweroff="sudo systemctl poweroff"
alias halt="sudo systemctl halt"
alias beep="sudo /usr/bin/beep"
alias dotfiles="/usr/bin/git --git-dir=$HOME/git/dotfiles/ --work-tree=$HOME"
alias mpv="mpv --input-ipc-server=/tmp/mpvsocket"
alias wego="wego -b worldweatheronline.com -wwo-api-key 584d19b6fbea4765b4c171118191301 -l parma"
alias tars="sftpman mount tars"
alias head="sed 11q"
alias compton="compton --config ~/.config/compton.conf -b --backend glx --blur-method kawase --blur-strength 4 --blur-background"
alias ls="exa"
alias kdenlive="flatpak run org.kde.kdenlive"
alias l="exa -la"
alias y="yay"
alias cat="bat"
alias fzf="fzf --bind=alt-j:down,alt-k:up,ctrl-d:page-down,ctrl-u:page-up"
alias vi="vim"
alias gchi="stack ghci"
alias ghc="stack ghc --"
alias vice="x64"

# The following lines were added by compinstall

zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle :compinstall filename '/home/giorgio/.zshrc'

autoload -Uz compinit
compinit
zstyle ':completion:*' menu select
zstyle ':completion::complete:*' gain-privileges 1
source zsh-syntax-highlighting.zsh
[[ -a /usr/share/doc/pkgfile/command-not-found.zsh ]] && source /usr/share/doc/pkgfile/command-not-found.zsh
zstyle ':completion:*' rehash true

source ~/.zsh/autojump.zsh
source zsh-autosuggestions.zsh
export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=24'
export ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE=20
export ZSH_AUTOSUGGEST_USE_ASYNC="true"
export HISTORY_SUBSTRING_SEARCH_ENSURE_UNIQUE="true"

# Vim keybindings
bindkey -v
export KEYTIMEOUT=1

source ~/.zsh/fzf-bindings.zsh
source ~/.zsh/fzf-completion.zsh

source zsh-history-substring-search.zsh
autoload edit-command-line
zle -N edit-command-line
bindkey -M vicmd '/' history-incremental-search-backward
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down
bindkey -M vicmd ' ' edit-command-line
bindkey "$terminfo[kcuu1]" history-substring-search-up
bindkey "$terminfo[kcud1]" history-substring-search-down
vim_ins_mode="%{$fg[cyan]%}[INS]%{$reset_color%}"
vim_cmd_mode="%{$fg[green]%}[CMD]%{$reset_color%}"
vim_mode=$vim_ins_mode
python_path="$(python3 -V | awk '/^Python / {print "python" substr($2,0,3)}')"

if [ -n "$SSH_CLIENT" ]; then
	function zle-keymap-select {
		vim_mode="${${KEYMAP/vicmd/${vim_cmd_mode}}/(main|viins)/${vim_ins_mode}}"
		PS1="${vim_mode}[ %n@%M | %~ ]%# "
		zle reset-prompt
	}
	zle -N zle-keymap-select

	function zle-line-finish {
		vim_mode=$vim_ins_mode
		PS1="${vim_mode}[ %{$fg[red]%}%n@%M%{$reset_color%} | %~ ]%# "
	}
	zle -N zle-line-finish

	function TRAPINT() {
		vim_mode=$vim_ins_mode
		return $(( 128 + $1 ))
	} 

	PS1="${vim_mode}[ %n@%M | %~ ]%# "
else
	powerline-daemon -q
	. /usr/lib/$python_path/site-packages/powerline/bindings/zsh/powerline.zsh
fi

stty -ixon	#disable annoying scroll lock on ctrl+s
