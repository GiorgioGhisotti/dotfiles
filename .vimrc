set nocompatible              " be iMproved, required
filetype off                  " required

set rtp+=~/.vim/bundle/LanguageClient-neovim
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'lervag/vimtex'
Plugin 'kkoomen/vim-doge'
Plugin 'vim-pandoc/vim-pandoc-syntax'
Plugin 'vim-pandoc/vim-pandoc'
Plugin 'autozimu/LanguageClient-neovim'
Plugin 'neovimhaskell/haskell-vim.git'
Plugin 'vim-erlang/vim-erlang-omnicomplete'
Plugin 'vim-erlang/vim-erlang-compiler'
Plugin 'vim-erlang/vim-erlang-runtime'
Plugin 'fatih/vim-go'
Plugin 'elmcast/elm-vim'
Plugin 'udalov/kotlin-vim'
Plugin 'hura/vim-asymptote'
Plugin 'Shougo/vimproc.vim'
Plugin 'idanarye/vim-vebugger'
Plugin 'w0rp/ale'
Plugin 'scrooloose/nerdtree'
Plugin 'Shougo/unite.vim'
Plugin 'thinca/vim-quickrun'
Plugin 'tpope/vim-surround'
Plugin 'klen/python-mode'
Plugin 'fs111/pydoc.vim'
Plugin 'cburroughs/pep8.py'
Plugin 'vim-scripts/a.vim'
Plugin 'airblade/vim-gitgutter'
Plugin 'tpope/vim-fugitive'
Plugin 'kien/ctrlp.vim'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plugin 'junegunn/fzf.vim'
Plugin 'elzr/vim-json'
"Plugin 'Valloric/YouCompleteMe'	"if you're using Arch you'll have fewer issues using the AUR package
call vundle#end()

set hidden

let g:LanguageClient_serverCommands = {
    \ 'haskell': ['hie-wrapper'],
    \ }


if !exists('g:ycm_semantic_triggers')
	let g:ycm_semantic_triggers = {}
endif
let g:ycm_semantic_triggers.tex = ['@']
let g:ycm_filetype_blacklist = {}
let g:pandoc#folding#level=100
let g:pandoc#syntax#codeblocks#embeds#langs = ['c']
let g:pandoc#spell#enabled = 0
let g:pandoc#syntax#conceal#use = 0
let g:pandoc#folding#fdc = 0

if !exists('g:ycm_semantic_triggers')
	let g:ycm_semantic_triggers = {}
endif
au VimEnter * let g:ycm_semantic_triggers.tex=g:vimtex#re#youcompleteme

let g:vimtex_view_automatic=0

" If you are using a console version of Vim, or dealing
" with a file that changes externally (e.g. a web server log)
" then Vim does not always check to see if the file has been changed.
" The GUI version of Vim will check more often (for example on Focus change),
" and prompt you if you want to reload the file.
"
" There can be cases where you can be working away, and Vim does not
" realize the file has changed. This command will force Vim to check
" more often.
"
" Calling this command sets up autocommands that check to see if the
" current buffer has been modified outside of vim (using checktime)
" and, if it has, reload it for you.
"
" This check is done whenever any of the following events are triggered:
" * BufEnter
" * CursorMoved
" * CursorMovedI
" * CursorHold
" * CursorHoldI
"
" In other words, this check occurs whenever you enter a buffer, move the cursor,
" or just wait without doing anything for 'updatetime' milliseconds.
"
" Normally it will ask you if you want to load the file, even if you haven't made
" any changes in vim. This can get annoying, however, if you frequently need to reload
" the file, so if you would rather have it to reload the buffer *without*
" prompting you, add a bang (!) after the command (WatchForChanges!).
" This will set the autoread option for that buffer in addition to setting up the
" autocommands.
"
" If you want to turn *off* watching for the buffer, just call the command again while
" in the same buffer. Each time you call the command it will toggle between on and off.
"
" WatchForChanges sets autocommands that are triggered while in *any* buffer.
" If you want vim to only check for changes to that buffer while editing the buffer
" that is being watched, use WatchForChangesWhileInThisBuffer instead.
"
command! -bang WatchForChanges                  :call WatchForChanges(@%,  {'toggle': 1, 'autoread': <bang>0})
command! -bang WatchForChangesWhileInThisBuffer :call WatchForChanges(@%,  {'toggle': 1, 'autoread': <bang>0, 'while_in_this_buffer_only': 1})
command! -bang WatchForChangesAllFile           :call WatchForChanges('*', {'toggle': 1, 'autoread': <bang>0})
" WatchForChanges function
"
" This is used by the WatchForChanges* commands, but it can also be
" useful to call this from scripts. For example, if your script executes a
" long-running process, you can have your script run that long-running process
" in the background so that you can continue editing other files, redirects its
" output to a file, and open the file in another buffer that keeps reloading itself
" as more output from the long-running command becomes available.
"
" Arguments:
" * bufname: The name of the buffer/file to watch for changes.
"     Use '*' to watch all files.
" * options (optional): A Dict object with any of the following keys:
"   * autoread: If set to 1, causes autoread option to be turned on for the buffer in
"     addition to setting up the autocommands.
"   * toggle: If set to 1, causes this behavior to toggle between on and off.
"     Mostly useful for mappings and commands. In scripts, you probably want to
"     explicitly enable or disable it.
"   * disable: If set to 1, turns off this behavior (removes the autocommand group).
"   * while_in_this_buffer_only: If set to 0 (default), the events will be triggered no matter which
"     buffer you are editing. (Only the specified buffer will be checked for changes,
"     though, still.) If set to 1, the events will only be triggered while
"     editing the specified buffer.
"   * more_events: If set to 1 (the default), creates autocommands for the events
"     listed above. Set to 0 to not create autocommands for CursorMoved, CursorMovedI,
"     (Presumably, having too much going on for those events could slow things down,
"     since they are triggered so frequently...)
function! WatchForChanges(bufname, ...)
  " Figure out which options are in effect
  if a:bufname == '*'
    let id = 'WatchForChanges'.'AnyBuffer'
    " If you try to do checktime *, you'll get E93: More than one match for * is given
    let bufspec = ''
  else
    if bufnr(a:bufname) == -1
      echoerr "Buffer " . a:bufname . " doesn't exist"
      return
    end
    let id = 'WatchForChanges'.bufnr(a:bufname)
    let bufspec = a:bufname
  end
  if len(a:000) == 0
    let options = {}
  else
    if type(a:1) == type({})
      let options = a:1
    else
      echoerr "Argument must be a Dict"
    end
  end
  let autoread    = has_key(options, 'autoread')    ? options['autoread']    : 0
  let toggle      = has_key(options, 'toggle')      ? options['toggle']      : 0
  let disable     = has_key(options, 'disable')     ? options['disable']     : 0
  let more_events = has_key(options, 'more_events') ? options['more_events'] : 1
  let while_in_this_buffer_only = has_key(options, 'while_in_this_buffer_only') ? options['while_in_this_buffer_only'] : 0
  if while_in_this_buffer_only
    let event_bufspec = a:bufname
  else
    let event_bufspec = '*'
  end
  let reg_saved = @"
  "let autoread_saved = &autoread
  let msg = "\n"
  " Check to see if the autocommand already exists
  redir @"
    silent! exec 'au '.id
  redir END
  let l:defined = (@" !~ 'E216: No such group or event:')
  " If not yet defined...
  if !l:defined
    if l:autoread
      let msg = msg . 'Autoread enabled - '
      if a:bufname == '*'
        set autoread
      else
        setlocal autoread
      end
    end
    silent! exec 'augroup '.id
      if a:bufname != '*'
        "exec "au BufDelete    ".a:bufname . " :silent! au! ".id . " | silent! augroup! ".id
        "exec "au BufDelete    ".a:bufname . " :echomsg 'Removing autocommands for ".id."' | au! ".id . " | augroup! ".id
        exec "au BufDelete    ".a:bufname . " execute 'au! ".id."' | execute 'augroup! ".id."'"
      end
        exec "au BufEnter     ".event_bufspec . " :checktime ".bufspec
        exec "au CursorHold   ".event_bufspec . " :checktime ".bufspec
        exec "au CursorHoldI  ".event_bufspec . " :checktime ".bufspec
      " The following events might slow things down so we provide a way to disable them...
      " vim docs warn:
      "   Careful: Don't do anything that the user does
      "   not expect or that is slow.
      if more_events
        exec "au CursorMoved  ".event_bufspec . " :checktime ".bufspec
        exec "au CursorMovedI ".event_bufspec . " :checktime ".bufspec
      end
    augroup END
    let msg = msg . 'Now watching ' . bufspec . ' for external updates...'
  end
  " If they want to disable it, or it is defined and they want to toggle it,
  if l:disable || (l:toggle && l:defined)
    if l:autoread
      let msg = msg . 'Autoread disabled - '
      if a:bufname == '*'
        set noautoread
      else
        setlocal noautoread
      end
    end
    " Using an autogroup allows us to remove it easily with the following
    " command. If we do not use an autogroup, we cannot remove this
    " single :checktime command
    " augroup! checkforupdates
    silent! exec 'au! '.id
    silent! exec 'augroup! '.id
    let msg = msg . 'No longer watching ' . bufspec . ' for external updates.'
  elseif l:defined
    let msg = msg . 'Already watching ' . bufspec . ' for external updates'
  end
  let @"=reg_saved
endfunction

let python_path=system("python3 -V | awk '/^Python / {print \"/usr/bin/python\" substr($2,0,3) \"/site-packages/powerline/bindngs/vim/\"}'")

packadd termdebug
map <C-g> :Termdebug<CR>
exe 'set rtp+=' . system("python3 -V | awk '/^Python / {print \"/usr/lib/python\" substr($2,0,3) \"/site-packages/powerline/bindings/vim/\"}'")
let g:plantuml_executable_script='plantuml'

augroup filetypedetect
au BufNewFile,BufRead *.mod  setf ampl
au BufNewFile,BufRead *.dat  setf ampl
au BufNewFile,BufRead *.ampl setf ampl
au BufNewFile,BufRead *.sctx setf sccharts
augroup END

autocmd FileType * exec("setlocal dictionary+=".$HOME."/.vim/dictionaries/".expand('<amatch>'))
set completeopt=menuone,longest,preview
set complete+=k

au Filetype haskell set omnifunc=LanguageClient#complete

" Use a good indentation system
set smartindent
set tabstop=4
set shiftwidth=4
set noexpandtab
set pastetoggle=<F7>

au Filetype haskell set expandtab

au Filetype sccharts set tabstop=2

" Enable mouse
"set mouse=a

" Ignore case when searching
set ignorecase

" Highlight search results
set hlsearch

" Show matching brackets when text indicator is over them
set showmatch

" Syntax highlighting
syntax enable
syntax on

" enable filetype detection and plugin loading
filetype plugin on

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Always show the status line
set laststatus=2

" Format the status line
set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ 

" Returns true if paste mode is enabled
function! HasPaste()
    if &paste
        return 'PASTE MODE  '
    en
    return ''
endfunction

map <C-y> 1k
map <C-e> 1j

nnoremap ; :

map j gj
map k gkzz

source ~/.vim/scrollfix.vim
let g:scrollfix=50
let g:fixeof=0
let g:scrollinfo=0

colorscheme dwarflord
map <C-t> :tabnew<CR>
map <C-l> :tabn<CR>
map <C-h> :tabp<CR>
map <C-j> :wincmd j<CR>
map <C-k> :wincmd k<CR>
command! -nargs=1 Ngrep vimgrep "<args>" ./*.*
map <C-f> :Ngrep 
nnoremap <F5> :w<CR> :silent make<CR> :redraw!<CR>
inoremap <F5> <Esc>:w<CR>:silent make<CR> :redraw!<CR>
vnoremap <F5> :<C-U>:w<CR>:silent make<CR> :redraw!<CR>
autocmd BufWritePost config.def.h !rm -f config.h && sudo make install
autocmd BufWritePost .Xresources* !xrdb % && python3 ~/.scripts/gen_pow_cols.py %
autocmd BufWritePost *.md silent !pandoc % --pdf-engine=xelatex --filter ~/.scripts/center_filter.py --filter pandoc-plantuml -o ./%:t:r.pdf > error.log 2>&1 3>&1 &
autocmd BufWritePost *.plantuml silent !plantuml % -tsvg -o "./" &
autocmd BufWritePost *.go :GoFmt
autocmd BufWritePost *.go silent :GoMetaLinter
autocmd Filetype python map <F5> <Esc>:w<cr> :!python3<space>%<CR>
autocmd Filetype haskell map <F5> <Esc>:w<cr> :!stack ghc -- --make<space>%<CR>
autocmd Filetype rust map <F4> :wall<cr> :!cargo build<cr>
autocmd Filetype rust map <F5> :!cargo run<cr>
autocmd BufWritePost *.asy silent !asy -f pdf % &
set number
set relativenumber

au BufEnter *.hs compiler ghc
let g:ale_pattern_options = {
\	'*\.c$': {'ale_enabled': 0},
\	'*\.cpp$': {'ale_enabled': 0},
\	'*\.h$': {'ale_enabled': 0},
\	'*\.hpp$': {'ale_enabled': 0},
\}
let g:ale_pattern_options_enabled = 1

if has('persistent_undo')      "check if your vim version supports it
	set undofile                 "turn on the feature  
	set undodir=$HOME/.vim/undo  "directory where the undo files will be stored
endif

let g:ycm_global_ycm_extra_conf = '~/.vim/.ycm_extra_conf.py'
let g:ycm_confirm_extra_conf = 0
let g:ycm_server_python_interpreter = '/usr/bin/python3'
let g:ycm_autoclose_preview_window_after_insertion = 1

let g:goyo_height = 100

""" DEVELOPMENT SPECIFIC OPTIONS
highlight ColorColumn ctermbg=0 ctermfg=7
if exists('+colorcolumn')
	set colorcolumn=80
else
	au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)
endif

" Source local .vimrc files if present
set exrc
set secure

let g:ale_enabled = 0
let g:ale_set_highlights = 0
let g:ale_lint_on_insert_leave = 1
set autoread
au FocusGained,BufEnter * :silent! !
let autoreadargs={'autoread':1} 
execute WatchForChanges("*",autoreadargs) 

let g:go_def_mapping_enabled = 0
if has("mouse_sgr")
    set ttymouse=sgr
else
    set ttymouse=xterm2
end

if !has('gui_running')
	map n <A-n>
	map 	<C-n>
endif

set conceallevel=0
