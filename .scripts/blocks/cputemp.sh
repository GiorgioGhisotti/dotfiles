#!/bin/sh

color=$(xrdb -query | grep '*color8'| awk '{print $NF}')
bgcolor=$(xrdb -query | grep '*color6'| awk '{print $NF}')
temp="$(sensors | awk '/^Package/ {print "CPU " substr($4,2,2) "⁰C"}')"
echo "<span foreground=\"$color\" background=\"$bgcolor\">$temp </span>"
