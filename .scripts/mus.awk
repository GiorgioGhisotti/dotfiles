#! /bin/awk -f
NR==1{
	if($1 != "volume:"){
		split($0,a," - ");
		if(length(a[2]) < 20){
			print a[2];
		}else{
			print substr(a[2],0,18) "...";
		}
	}
}
