#!/bin/sh
case "$(printf "a selected area\\nfull screen" | dmenu -l 6 -i -p "Screenshot which area?")" in
	"a selected area") scrot -q 100 -s /tmp/area.png && xclip -selection clipboard -t image/png -i /tmp/area.png && notify-send -u "low" -t 2000 "Screenshot copied to clipboard!" ;;
	"full screen") scrot -d 1 -c -q 100 /tmp/fullscreen.png && xclip -selection clipboard -t image/png -i /tmp/fullscreen.png && notify-send -u "low" -t 2000 "Full screen screenshot copied to clipboard!" ;;
esac
