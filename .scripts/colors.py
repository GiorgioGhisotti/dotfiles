#!/usr/bin/python
import sys

if(len(sys.argv) <2):
    print("Usage: %s color" %sys.argv[0])
    exit(0)

colorset=0
color=int(sys.argv[1])

cols = [
    [
	# solarized dark */
        "#073642",  #  0: black    */
        "#dc322f",  #  1: red      */
        "#859900",  #  2: green    */
        "#b58900",  #  3: yellow   */
        "#268bd2",  #  4: blue     */
        "#d33682",  #  5: magenta  */
        "#2aa198",  #  6: cyan     */
        "#eee8d5",  #  7: white    */
        "#002b36",  #  8: brblack  */
        "#cb4b16",  #  9: brred    */
        "#586e75",  # 10: brgreen  */
        "#657b83",  # 11: bryellow */
        "#839496",  # 12: brblue   */
        "#6c71c4",  # 13: brmagenta*/
        "#93a1a1",  # 14: brcyan   */
        "#fdf6e3",  # 15: brwhite  */
        "black"
    ],[
        # solarized light */
        "#eee8d5",  #  0: black    */
        "#dc322f",  #  1: red      */
        "#859900",  #  2: green    */
        "#b58900",  #  3: yellow   */
        "#268bd2",  #  4: blue     */
        "#d33682",  #  5: magenta  */
        "#2aa198",  #  6: cyan     */
        "#073642",  #  7: white    */
        "#fdf6e3",  #  8: brblack  */
        "#cb4b16",  #  9: brred    */
        "#93a1a1",  # 10: brgreen  */
        "#839496",  # 11: bryellow */
        "#657b83",  # 12: brblue   */
        "#6c71c4",  # 13: brmagenta*/
        "#586e75",  # 14: brcyan   */
        "#002b36",  # 15: brwhite  */
        "black"
    ],[
        "black",
        "red2",
        "green2",
        "yellow2",
        "cyan2",
        "magenta2",
        "cyan2",
        "gray90",

        "gray50",
        "red",
        "green",
        "yellow",
        "#ffffff",
        "cyan3",
        "cyan",
        "#2222cc",
        "#cccccc"
    ],[

        "#2B2B2B",
        "#870000",
        "#5F875F",
        "#875F00",
        "#005FAF",
        "#5F5F87",
        "#008787",
        "#818181",

        "#414141",
        "#D70000",
        "#AFD7AF",
        "#D7AF00",
        "#00AFFF",
        "#AFAFD7",
        "#00D7D7",
        "#CECECE",

        "#cccccc"
    ],[
        "#151515",
        "#ac4142",
        "#90a959",
        "#f4bf75",
        "#6a9fb5",
        "#aa759f",
        "#75b5aa",
        "#d0d0d0",

        "#505050",
        "#ac4142",
        "#90a959",
        "#f4bf75",
        "#6a9fb5",
        "#aa759f",
        "#75b5aa",
        "#f5f5f5",
        "#cccccc"
    ],[
        "black",
        "red3",
        "green3",
        "yellow3",
        "#6a9fb5",
        "magenta3",
        "cyan3",
        "green3",

        "gray50",
        "red",
        "green",
        "yellow",
        "#5c5cff",
        "magenta",
        "cyan",
        "white",
        "green3"
    ],[
        "#332233",
        "#ac4142",
        "#90a959",
        "#f4bf75",
        "#6a9fb5",
        "#aa759f",
        "#75b5aa",
        "#d0d0d0",

        "#505050",
        "#ac4142",
        "#90a959",
        "#f4bf75",
        "#6a9fb5",
        "#aa759f",
        "#75b5aa",
        "#f5f5f5",
        "#cccccc"
    ]
]
if(len(cols[colorset]) > color):
    print(cols[colorset][color])
else:
    print("#00000000") 

