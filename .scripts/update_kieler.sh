url="http://rtsys.informatik.uni-kiel.de/~kieler/files/nightly/sccharts/"
arch=$(uname -m)
file=$(wget -O - $url | \
grep -o '<a href=['"'"'"][^"'"'"']*['"'"'"]' | \
sed -e 's/^<a href=["'"'"']//' -e 's/["'"'"']$//' |\
grep $arch | grep linux)
echo "$url$file"
