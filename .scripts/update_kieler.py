#!/bin/python3

from bs4 import BeautifulSoup
import urllib.request
import re
import subprocess
import fileinput
from pathlib import Path


def getPackage(url, arch, tmpdir):
    website = urllib.request.urlopen(url)
    soup = BeautifulSoup(website.read(), features="lxml")
    debs = list(link.get("href") for link in soup.findAll("a"))
    print(debs)
    ex = "sccharts_rca_nightly_[.0-9]+-linux.gtk." + arch + ".tar.gz"
    package = [x for x in debs if re.search(ex, x)][0]
    print(package)
    subprocess.run(["wget", "-nc", url + package,
                    "-O", tmpdir + package])
    return package


if __name__ == "__main__":
    arch = "x86_64"
    tmpdir = "/tmp/"
    url = "http://rtsys.informatik.uni-kiel.de/%7Ekieler/files/nightly/sccharts/"

    kielerpath = str(Path.home()) + "/.local/share/"

    package = getPackage(url, arch, tmpdir)

    subprocess.run(["tar", "-xvf", tmpdir + package, "-C", tmpdir])

    subprocess.run(["rm", "-rf", kielerpath + "kieler_old"])

    subprocess.run(["rm", "-rf",
                    kielerpath + "kieler_headless"])

    subprocess.run(["mv", kielerpath + "kieler",
                    kielerpath + "kieler_old"])

    subprocess.run(["cp", "-R", tmpdir + "kieler",
                    kielerpath + "kieler"])

    with fileinput.FileInput(kielerpath + "kieler/kieler.ini",
                             inplace=True) as ini:
        for line in ini:
            line = line.replace("-Xss4m", "-Xss256m")
            line = line.replace("-Xms512m", "-Xms4096m")
            line = line.replace("-Xmn256m", "-Xmn1024m")
            line = line.replace("-Xmx1024m", "-Xmx4096m")
            print(line, end='')

    subprocess.run(["cp", "-R", kielerpath + "kieler",
                    kielerpath + "kieler_headless"])

    with fileinput.FileInput(kielerpath + "kieler_headless/kieler.ini",
                             inplace=True) as ini:
        for line in ini:
            line = line.replace("-startup",
                                "-application\n" +
                                "de.cau.cs.kieler.sccharts.ui.application." +
                                "SCChartFileRenderingApplication\n" +
                                "-noSplash\n-startup")
            print(line, end='')
