#!/bin/sh

[ ! -d ~/Bluetooth ] && mkdir -p ~/Bluetooth
fusermount -u ~/Bluetooth
obexfs -b $1 ~/Bluetooth
notify-send -u "low" -t 2000 "Bluetooth device $1 mounted!"
dolphin ~/Bluetooth
