#!/bin/sh

#setup git - required for recovering dotfiles and more
sudo pacman -S git gcc --needed --noconfirm

mkdir -p ~/git
cd ~/git
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -sri
cd

#setup dotfiles repository and fetch dotfiles
git clone --bare https://gitlab.com/GiorgioGhisotti/dotfiles ~/git/dotfiles

alias dotfiles='/usr/bin/git --git-dir=$HOME/git/dotfiles/ --work-tree=$HOME'

dotfiles checkout

#set shell to zsh
chsh -s /bin/zsh

#get st
cd ~/git
git clone https://gitlab.com/GiorgioGhisotti/st
cd st
make
sudo make install
cd

cd git
git clone https://gist.github.com/GiorgioGhisotti/9d4b9bfbbe2381a37c1bb1db9d37e3af pkgs
cd pkgs
yay -S --needed --noconfirm $(tr '\n' ' ' < Marvin.pacmanity)
cd

sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
