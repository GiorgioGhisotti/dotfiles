# vim:fileencoding=utf-8:noet
from __future__ import (
    unicode_literals,
    division,
    absolute_import,
    print_function)

import os
import subprocess
from pathlib import Path

from multiprocessing import cpu_count as _cpu_count

from powerline.lib.threaded import ThreadedSegment
from powerline.lib import add_divider_highlight_group
from powerline.segments import with_docstring


def volume():
    return ""


def system_load(pl):
    return volume()
