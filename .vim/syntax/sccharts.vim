" Vim syntax file
" Language:     SCCharts
" Maintainer:   Giorgio Ghisotti
"               g.ghisotti@gmail.com
" Version:      0.1

if exists("b:current_syntax")
	finish
endif

" Keywords
syn keyword Conditional		if auto entry else
syn keyword Repeat			go to for join
syn keyword Statement		do is return
syn keyword Type			int bool float string host signal input output void
syn keyword Special			initial immediate final delayed const
syn keyword Function		state region scchart import method
syn match Operator			"+"
syn match Operator			"!="
syn match Operator			">="
syn match Operator			"<="
syn match Operator			">"
syn match Operator			"<"
syn match Operator			"=<"
syn match Operator			"=>"
syn match Operator			"-"
syn match Operator			"/"
syn match Operator			"="
syn match Constant			"\<[A-Z_0-9]\+\>"
" Integer with - + or nothing in front
syn match Number '\d\+'
syn match Number '[-+]\d\+'

" Floating point number with decimal no E or e 
syn match Number '[-+]\d\+\.\d*'

" Floating point like number with E and no decimal point (+,-)
syn match Number '[-+]\=\d[[:digit:]]*[eE][\-+]\=\d\+'
syn match Number '\d[[:digit:]]*[eE][\-+]\=\d\+'

" Floating point like number with E and decimal point (+,-)
syn match Number '[-+]\=\d[[:digit:]]*\.\d*[eE][\-+]\=\d\+'
syn match Number '\d[[:digit:]]*\.\d*[eE][\-+]\=\d\+'

" Comments are everything to the right of a #
syntax match   Comment              /\/\/.*/

" Blocks
syntax region  Block                start=/{/    end=/}/    contains=ALL
syntax region  String                start=/\v"/    end=/\v"/
syntax region  CommentBlock         start=/\/\*/ end=/\*\// keepend
