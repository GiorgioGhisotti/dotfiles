set background=dark
hi clear
if exists("syntax_on")
  syntax reset
endif
let g:colors_name = "dwarflord"
hi Normal	ctermfg=12		guifg=cyan			guibg=black
hi Visual	ctermbg=0
hi SpellBad	ctermbg=3	ctermfg=13
hi Comment	term=bold		ctermfg=13		guifg=#80a0ff
hi Constant	term=underline	ctermfg=6		guifg=Magenta
hi Special	term=bold		ctermfg=5	guifg=Red
hi Identifier term=underline	cterm=bold			ctermfg=11 guifg=#40ffff
hi Statement term=bold		ctermfg=4 gui=bold	guifg=#aa4444
hi PreProc	term=underline	ctermfg=5	guifg=#ff80ff
hi Type	term=underline		ctermfg=6	guifg=#60ff60 gui=bold
hi Function	term=bold		ctermfg=2 guifg=White
hi Title	ctermfg=2 guifg=White
hi Folded	ctermfg=0	ctermbg=6
hi FoldColumn	ctermfg=0	ctermbg=6
hi Conceal	ctermfg=6	ctermbg=258
hi Repeat	cterm=bold	ctermfg=11		guifg=white
hi Operator	term=bold	ctermfg=13			guifg=Red
hi Ignore				ctermfg=13		guifg=bg
hi Error	term=reverse ctermbg=13 ctermfg=12 guibg=Red guifg=White
hi SpellCap	term=reverse ctermbg=9 guibg=Red guifg=White
hi Todo	term=standout ctermbg=3 ctermfg=8 guifg=Blue guibg=Yellow
hi LineNr ctermfg=2
hi CursorLineNr ctermfg=11

" Common groups that link to default highlighting.
" You can specify other highlighting easily.
hi link CommentBlock	Comment
hi link String	Constant
hi link Character	Constant
hi link Number	Constant
hi link Boolean	Constant
hi link Float		Number
hi link Conditional	Repeat
hi link Label		Statement
hi link Keyword	Statement
hi link Exception	Statement
hi link Include	PreProc
hi link Define	PreProc
hi link Macro		PreProc
hi link PreCondit	PreProc
hi link StorageClass	Type
hi link Structure	Type
hi link Typedef	Type
hi link Tag		Special
hi link SpecialChar	Special
hi link Delimiter	Special
hi link SpecialComment Special
hi link Debug		Special
