if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
	export QT_STYLE_OVERRIDE=kvantum
	export QT_QPA_PLATFORMTHEME="qt5ct" 
	exec startx
fi

export PATH="$HOME/.cargo/bin:$PATH"
